// junior.cpp

#include "SerialPort.h"
#include <iostream>
#include <string>
#include <thread>  // For simulating command execution

const uint8_t JUNIOR_ADDRESS = 1;  // Change this to the Junior's address
const uint32_t BAUDRATE = 9600;

enum JuniorState {
    STATE_NOT_INITIALIZED,
    STATE_IDLE,
    STATE_BUSY
};

// Define Junior device types
enum JuniorDeviceType {
    JUNIOR_MOTOR = 1,
    JUNIOR_PUMP = 2,
    // Add more device types as needed
};

class Junior {
public:
    Junior(const char* rxPort, const char* txPort, uint8_t address, JuniorDeviceType deviceType);

    void run();

private:
    JuniorState state;
    SerialPort serialPort;
    uint8_t address;
    JuniorDeviceType deviceType;

    void handleCommands();
    void executeMotorCommand();
    void executePumpCommand();
    void sendStateUpdate();
};

Junior::Junior(const char* rxPort, const char* txPort, uint8_t address, JuniorDeviceType deviceType)
    : state(STATE_NOT_INITIALIZED), address(address), deviceType(deviceType)
{
    serialPort.connect(rxPort, BAUDRATE);
    serialPort.registerReceiveCallback([&](const char* msg, uint16_t size) {
        // Handle received messages from Senior
        // Implement message parsing and execute commands
        // For simplicity, assume the received message is a MotorCommand or PumpCommand
        if (state == STATE_IDLE) {
            if (deviceType == JUNIOR_MOTOR && strcmp(msg, "MotorCommand") == 0) {
                executeMotorCommand();
            } else if (deviceType == JUNIOR_PUMP && strcmp(msg, "PumpCommand") == 0) {
                executePumpCommand();
            }
        }
    });
}

void Junior::handleCommands() {
    // Implement logic to handle various commands
    // For simplicity, we will simulate command execution

    if (state == STATE_IDLE) {
        if (deviceType == JUNIOR_MOTOR) {
            std::cout << "Junior (Address: " << static_cast<int>(address) << ") executing MotorCommand" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(2));  // Simulate command execution time
        } else if (deviceType == JUNIOR_PUMP) {
            std::cout << "Junior (Address: " << static_cast<int>(address) << ") executing PumpCommand" << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(1));  // Simulate command execution time
        }
        state = STATE_IDLE;  // Command execution completed, return to IDLE state
    }
}

void Junior::executeMotorCommand() {
    // Implement logic to execute a MotorCommand
    // Change state to STATE_BUSY while executing the command
    state = STATE_BUSY;
    handleCommands();  // Simulate execution for simplicity
}

void Junior::executePumpCommand() {
    // Implement logic to execute a PumpCommand
    // Change state to STATE_BUSY while executing the command
    state = STATE_BUSY;
    handleCommands();  // Simulate execution for simplicity
}

void Junior::sendStateUpdate() {
    // Send a status update to the Senior
    // Include Junior's address, device type, and state
    std::string message = "Address: " + std::to_string(address) + ", Device Type: " +
        std::to_string(deviceType) + ", State: " + (state == STATE_IDLE ? "IDLE" : "BUSY");
    serialPort.sendMessage(const_cast<char*>(message.c_str()), message.length());
}

void Junior::run() {
    while (true) {
        handleCommands();
        sendStateUpdate();
        // Implement other logic as needed
    }
}

int main(int argc, char** argv) {
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " <RX port> <TX port> <address> <device type>" << std::endl;
        return 1;
    }

    const char* rxPort = argv[1];
    const char* txPort = argv[2];
    uint8_t address = std::atoi(argv[3]);
    JuniorDeviceType deviceType = static_cast<JuniorDeviceType>(std::atoi(argv[4]));

    Junior junior(rxPort, txPort, address, deviceType);
    junior.run();

    return 0;
}

