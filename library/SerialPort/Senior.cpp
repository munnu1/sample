// senior.cpp

#include "Senior.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <json/json.h> // You may need to install the jsoncpp library

Senior::Senior(const char* rxPort, const char* txPort)
    : state(WAITING_FOR_CONFIG)
{
    serialPort.connect(rxPort, BAUDRATE);
    serialPort.registerReceiveCallback([&](const char* msg, uint16_t size) {
        // Handle received messages from Juniors
        // Update Junior states, configuration, etc.
    });
}

void Senior::initialize() {
    // Read Junior configuration files from the 'devices' directory
    std::string devicesDirectory = "devices";
    std::vector<std::string> configFiles;
    
    // Load JSON configuration files
    for (const auto& entry : std::filesystem::directory_iterator(devicesDirectory)) {
        if (entry.is_regular_file() && entry.path().extension() == ".json") {
            configFiles.push_back(entry.path().string());
        }
    }

    // Parse JSON configuration files
    for (const auto& configFile : configFiles) {
        std::ifstream ifs(configFile);
        Json::Value root;
        Json::CharReaderBuilder readerBuilder;
        std::string errs;
        Json::parseFromStream(readerBuilder, ifs, &root, &errs);

        // Extract Junior configuration data and populate data structures
        uint8_t address = root["address"].asUInt();
        juniorAddresses.push_back(address);

        // Additional configuration data handling
    }

    // Transition to CONFIGURING state
    state = CONFIGURING;
}

void Senior::configureJuniors() {
    // Send ConfigureCommand to each Junior with their respective configuration
    // Wait for all Juniors to be in STATE_IDLE
    // Transition to IN_PRODUCTION state

    bool allIdle = false;

    while (!allIdle) {
        allIdle = true;

        for (const uint8_t& address : juniorAddresses) {
            // Construct and send ConfigureCommand to Junior with 'address'
            // Handle responses from Juniors and update 'allIdle'
        }

        // Check if all Juniors are in STATE_IDLE
    }

    // Transition to IN_PRODUCTION state
    state = IN_PRODUCTION;
}

void Senior::pollJuniors() {
    // Regularly poll attributes from each Junior
    // Update and manage Junior states and data as needed

    while (state == IN_PRODUCTION) {
        for (const uint8_t& address : juniorAddresses) {
            // Poll attributes from Junior with 'address'
            // Update and manage Junior states and data
        }

        // Implement your polling logic (e.g., sleep for a specific interval)
    }
}

void Senior::run() {
    while (true) {
        switch (state) {
            case WAITING_FOR_CONFIG:
                initialize();
                break;
            case CONFIGURING:
                configureJuniors();
                break;
            case IN_PRODUCTION:
                pollJuniors();
                break;
        }
        // Add any other necessary logic
    }
}

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <RX port> <TX port>" << std::endl;
        return 1;
    }

    Senior senior(argv[1], argv[2]);
    senior.run();

    return 0;
}

